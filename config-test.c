
#include "config.h"
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#define assert(x)                                                              \
	if (!(x))                                                              \
	__builtin_trap()

void (*update_block)(const void *__src, void *__dst, size_t __n);
void (*read_block)(void *__dst, const void *__src, size_t __n);

void eeprom_update_block(const void *__src, void *__dst, size_t __n) {
	update_block(__src, __dst, __n);
}
void eeprom_read_block(void *__dst, const void *__src, size_t __n) {
	read_block(__dst, __src, __n);
}

struct config emptyconfig;

void read_block_test1(void *dst, const void *src, size_t n) {
	struct config stored = {0x34};
	memmove(dst, &stored, n);
}

void test1(void) {
	read_block = read_block_test1;
	update_block = 0;
	config = emptyconfig;
	config_init();
	assert(config.midi_channel == 0x4);
}

struct config config_test2;
void update_block_test2(const void *src, void *dst, size_t n) {
	memmove(&config_test2, src, n);
}

void test2(void) {
	char buf[] = "\xf0`CrowBX\x56\xf7";
	midi_parser p = {0};
	midi_message msg;
	int n = sizeof(buf) - 1, i;

	read_block = 0;
	update_block = update_block_test2;
	config = emptyconfig;
	config_test2 = emptyconfig;

	for (i = 0; i < n; i++)
		if (msg = midi_read(&p, buf[i]), msg.status)
			config_handle_sysex(&msg);

	assert(config.midi_channel == 6);
	assert(config_test2.midi_channel == 6);
}

int main(void) {
	test1();
	test2();
	puts("ok");
}
