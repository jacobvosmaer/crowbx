#include "config.h"
#include "array_size.h"
#include <string.h>

#ifdef TEST
#define EEMEM
void eeprom_update_block(const void *__src, void *__dst, size_t __n);
void eeprom_read_block(void *__dst, const void *__src, size_t __n);
#else
#include <avr/eeprom.h>
#endif

struct config config = {0};

static EEMEM struct config stored_config;

void config_init(void) {
	eeprom_read_block(&config, &stored_config, sizeof(config));
	config.midi_channel &= 0xf;
}

static const char sysex_header[] = "`CrowBX";
enum {
	SYSEX_HEADER_LENGTH = sizeof(sysex_header) - 1,
	SYSEX_MSG_LENGTH = SYSEX_HEADER_LENGTH + 1
};

static struct {
	/* Store 1 byte extra so that we can determine if the incoming
	 * sysex stream is too long. */
	uint8_t buffer[SYSEX_MSG_LENGTH + 1];
	uint8_t len;
} sysex;

void config_handle_sysex(midi_message *msg) {
	switch (msg->status) {
	case MIDI_SYSEX:
		if (msg->data[1] == MIDI_SYSEX)
			sysex.len = 0;
		if (sysex.len < ARRAY_SIZE(sysex.buffer))
			sysex.buffer[sysex.len++] = msg->data[0];
		break;
	case MIDI_SYSEX_END:
		if ((sysex.len == SYSEX_MSG_LENGTH) &&
		    !memcmp(sysex.buffer, sysex_header, SYSEX_HEADER_LENGTH)) {
			uint8_t *b = sysex.buffer + SYSEX_HEADER_LENGTH;
			config.midi_channel = *b++ & 0xf;
			eeprom_update_block(&config, &stored_config,
					    sizeof(config));
		}
		break;
	}
}
