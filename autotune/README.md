# autotune board

This is firmware for the microcontroller that controls the autotune
facility of my CrowBX. See this [web
page](https://crowbx-6d48f4aa5932532a5cd4216f.s3.amazonaws.com/autotune.html).
