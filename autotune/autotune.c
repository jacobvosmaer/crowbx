#include "array_size.h"
#include "config.h"
#include "midi.h"
#include "pin.h"

#include <avr/eeprom.h>
#include <avr/io.h>
#include <util/delay.h>

pin dacs[] = {
    {&PORTC, &DDRC, PORTC2},
    {&PORTC, &DDRC, PORTC1},
};

void spi_send(uint8_t data) {
	SPDR = data;
	while (!(SPSR & (1 << SPIF)))
		;
}

enum { NUM_VOICES = 8, NUM_OSCS = 2 };

void dac_send(uint8_t voice, uint8_t osc, uint16_t data) {
	pin *dac_sync;
	uint8_t channel, command;

	if ((voice >= NUM_VOICES) || (osc >= NUM_OSCS))
		return;

	dac_sync = &dacs[voice / 4];
	channel = osc + 2 * (voice % 4);
	command = 0x30 | channel;

	pin_off(dac_sync);
	spi_send(command);
	spi_send(data >> 8);
	spi_send(data & 0xff);
	pin_on(dac_sync);
}

void dac_init(void) {
	enum { SDO = PORTB3, SCK = PORTB5, CS = PORTB2 };
	uint8_t i;

	for (i = 0; i < ARRAY_SIZE(dacs); i++) {
		pin_init(&dacs[i]);
		pin_on(&dacs[i]);
	}
	_delay_ms(1); /* let dacs boot up */
	DDRB |= (1 << SDO) | (1 << SCK) | (1 << CS);
	SPCR = (1 << SPE) | (1 << MSTR) | (1 << CPOL);
	SPSR = (1 << SPI2X);
}

void uart_init(void) {
	enum { ubrr = (F_CPU / 16 / 31250) - 1 };
	UBRR0H = ubrr >> 8;
	UBRR0L = ubrr & 0xff;
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);

	/* Enable pull-up resistor on RX pin */
	PORTD |= (1 << PORTD0);
}

void timer_init(void) { TCCR1B = (1 << ICNC1) | (1 << CS11); }

uint16_t timer_input_capture(void) {
	TIFR1 |= (1 << ICF1);
	while (!(TIFR1 & (1 << ICF1)))
		;
	return ICR1;
}

enum { MODE_ECHO, MODE_TUNING };
static uint8_t mode = MODE_ECHO;

void uart_tx(uint8_t c) {
	while (!(UCSR0A & (1 << UDRE0)))
		;
	UDR0 = c;
}

uint8_t uart_read(uint8_t *c) {
	uint8_t status;

	do
		status = UCSR0A;
	while (!(status & (1 << RXC0)));
	*c = UDR0;

	if (status & ((1 << FE0) | (1 << DOR0) | (1 << UPE0)))
		return 0;

	if (mode == MODE_ECHO)
		uart_tx(*c);

	return 1;
}

uint16_t tune_vco(uint8_t voice, uint8_t osc) {
	const double c_hz = 261.626;
	const uint16_t target_ticks = (F_CPU / 8.0 / c_hz);
	uint16_t value, t0, t1, bit;

	/* MIDI note on, velocity 127 */
	uart_tx(MIDI_NOTE_ON | config.midi_channel);
	uart_tx(voice);
	uart_tx(127);

	value = 0;
	for (bit = (1 << 15); bit; bit >>= 1) {
		dac_send(voice, osc, value | bit);

		_delay_ms(10);
		t0 = timer_input_capture();
		t1 = timer_input_capture();
		if ((t1 - t0) >= target_ticks)
			value |= bit;
	}

	dac_send(voice, osc, value);
	return value;
}

static uint16_t EEMEM tune_stored_values[NUM_OSCS][NUM_VOICES];
static uint16_t tune_values[NUM_OSCS][NUM_VOICES];

void handle_channel_message(midi_message *msg) {
	uint8_t v, osc;

	if ((msg->status & 0xf) != config.midi_channel)
		return;

	switch (msg->status & 0xf0) {
	case MIDI_NOTE_ON:
		if (msg->data[1] && (mode == MODE_TUNING)) {
			osc = 1 & (msg->data[0] >> 6);
			for (v = 0; v < NUM_VOICES; v++)
				tune_values[osc][v] = tune_vco(v, osc);
			eeprom_update_block(tune_values[osc],
					    tune_stored_values[osc],
					    sizeof(tune_values[0]));
		}
		break;
	case MIDI_PROGRAM_CHANGE:
		if (msg->data[0] == 127) {
			mode = MODE_TUNING;
		} else if (mode == MODE_TUNING) {
			mode = MODE_ECHO;
			uart_tx(MIDI_PROGRAM_CHANGE | config.midi_channel);
			uart_tx(msg->data[0]);
		}
		break;
	}
}

int main(void) {
	uint8_t v, osc;
	midi_parser parser = {0};

	uart_init();
	dac_init();
	timer_init();
	config_init();

	eeprom_read_block(tune_values, tune_stored_values, sizeof(tune_values));
	for (osc = 0; osc < NUM_OSCS; osc++)
		for (v = 0; v < NUM_VOICES; v++)
			dac_send(v, osc, tune_values[osc][v]);

	for (;;) {
		uint8_t b;
		if (uart_read(&b)) {
			midi_message msg = midi_read(&parser, b);
			if (msg.status >= MIDI_SYSEX)
				config_handle_sysex(&msg);
			else if (msg.status)
				handle_channel_message(&msg);
		}
	}
}
