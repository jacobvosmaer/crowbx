PRG            = autotune
OBJ            = autotune.o midi.o config.o pin.o
MCU_TARGET = atmega328p

OPTIMIZE       = -Os -flto

DEFS           = -DF_CPU=16000000L
LIBS           =

CC             = avr-gcc

# min-pagesize is because of https://gcc.gnu.org/bugzilla/show_bug.cgi?id=105523
override CFLAGS        = -g -Wall -Wextra -Werror $(OPTIMIZE) -std=gnu89 -pedantic -mmcu=$(MCU_TARGET) --param=min-pagesize=0 $(DEFS) -I.. -I../midiparser
override LDFLAGS       = -Wl,-Map,$(PRG).map

OBJCOPY        = avr-objcopy
OBJDUMP        = avr-objdump

all: $(PRG).elf lst text

$(PRG).elf: $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

config.o: ../config.c
	$(CC) $(CFLAGS) -c -o $@ $^

pin.o: ../pin.c
	$(CC) $(CFLAGS) -c -o $@ $^

midi.o: ../midiparser/midi.c
	$(CC) $(CFLAGS) -c -o $@ $^

clean:
	rm -rf *.o $(PRG).elf *.eps *.png *.pdf *.bak 
	rm -rf *.lst *.map $(EXTRA_CLEAN_FILES)

AVRDUDE_PORT ?= /dev/tty.usbserial-*
upload: all
	avrdude -p m328p -b 57600 -D -c arduino -P $(AVRDUDE_PORT) -U flash:w:$(PRG).hex:i 

lst:  $(PRG).lst

%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

text: hex bin srec
	wc -c $(PRG).bin

hex:  $(PRG).hex
bin:  $(PRG).bin
srec: $(PRG).srec
syx:  $(PRG).syx

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

%.srec: %.elf
	$(OBJCOPY) -j .text -j .data -O srec $< $@

%.bin: %.elf
	$(OBJCOPY) -j .text -j .data -O binary $< $@

format:
	clang-format -i -style=file *.c
