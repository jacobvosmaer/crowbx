#ifndef ARRAY_SIZE_H
#define ARRAY_SIZE_H

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

#endif
