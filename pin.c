#include "pin.h"

void pin_init(pin *p) { *(p->ddr) |= 1 << (p->bit); }
void pin_on(pin *p) { *(p->port) |= 1 << (p->bit); }
void pin_off(pin *p) { *(p->port) &= ~(1 << (p->bit)); }
