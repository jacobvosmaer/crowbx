#include "list.h"

void l_push(list *l, uint8_t x) {
	uint8_t i;

	if (l->len >= l->cap)
		return;

	for (i = l->len; i; i--)
		l->array[i] = l->array[i - 1];
	l->array[0] = x;
	l->len++;
}

void l_delete(list *l, uint8_t x) {
	uint8_t i = 0;
	while (i < l->len)
		if (l->array[i] == x)
			l_delete_at(l, i);
		else
			i++;
}

void l_delete_at(list *l, uint8_t i) {
	uint8_t j;

	if (i >= l->len)
		return;

	for (j = i + 1; j < l->len; j++)
		l->array[j - 1] = l->array[j];
	l->len--;
}

void l_set_len(list *l, uint8_t len) {
	l->len = len;
	if (l->len > l->cap)
		l->len = l->cap;
}

void l_copy(list *dst, list *src) {
	uint8_t i;

	l_set_len(dst, src->len);
	for (i = 0; i < dst->len; i++)
		dst->array[i] = src->array[i];
}
