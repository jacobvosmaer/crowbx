#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#include "array_size.h"
#include "config.h"
#include "list.h"
#include "midi.h"
#include "pin.h"
#include "scale.h"

enum { NUM_SEMITONES = 12 }; /* Number of notes in 12tet scale */

/*
 * The DAC IC (AD5676R) provides accurate-enough offset and scaling from
 * 0-2.5V across all outputs. However, we do not use the DAC output
 * directly. It passes through a non-inverting opamp amplifier (using
 * LT1014s) to increase the range to about 0-7.8V (feedback resistors of
 * 10K and 4K7). Even though we have a precision opamp and 0.1%
 * resistors, the opamp amplifier stage introduces offset and scaling
 * error.
 *
 * To prevent accumulation of scaling error across multiple octaves we use a
 * specific octave step value for each DAC, based on measurements of the
 * hardware. The offset error of the opamp is automatically corrected by the
 * autotune board.
 */

/* Struct dac represents a single DAC+opamp pair. */
struct dac {
	/*
	 * Because of reasons, the mapping from DAC channels to voice card slots
	 * is not regular. The channel field maps the voice number to the
	 * correct DAC channel.
	 */
	const uint8_t channel;

	/*
	 * Octave corresponds to a 1V increase on each DAC/opamp pair.
	 */
	const uint16_t octave;
};

/* voicetab represents the physical hardware layout and parameters */
struct {
	pin gate;
	struct dac dac;
} voicetab[] = {
    {{&PORTC, &DDRC, 0}, {2, 8381}}, {{&PORTC, &DDRC, 1}, {3, 8381}},
    {{&PORTC, &DDRC, 2}, {4, 8383}}, {{&PORTC, &DDRC, 3}, {5, 8382}},
    {{&PORTD, &DDRD, 3}, {0, 8382}}, {{&PORTD, &DDRD, 4}, {1, 8380}},
    {{&PORTD, &DDRD, 5}, {7, 8382}}, {{&PORTD, &DDRD, 6}, {6, 8382}},
};

const uint8_t num_voices = ARRAY_SIZE(voicetab);

void spi_send(uint8_t data) {
	SPDR = data;
	while (!(SPSR & (1 << SPIF)))
		;
}

pin dac_sync = {&PORTB, &DDRB, PORTB1};

void dac_set(uint8_t v, uint16_t data) {
	const uint16_t dac_max = 0xf3ff;

	if (v >= num_voices)
		return;

	/* Prevent mysterious lock-up behavior for very high pitch */
	if (data > dac_max)
		data = dac_max;

	pin_off(&dac_sync);
	spi_send(0x30 | voicetab[v].dac.channel);
	spi_send(data >> 8);
	spi_send(data & 0xff);
	pin_on(&dac_sync);
}

void dac_init(void) {
	enum { SDO = PORTB3, SCK = PORTB5, CS = PORTB2 };

	pin_init(&dac_sync);
	pin_on(&dac_sync);
	_delay_ms(1); /* let dac boot up */

	/* Enable SPI peripheral */
	DDRB |= (1 << SDO) | (1 << SCK) | (1 << CS);
	SPCR |= (1 << SPE) | (1 << MSTR) | (1 << CPOL);
	SPSR |= (1 << SPI2X);
}

void timer_init(void) { TCCR1B = (1 << CS11) | (1 << CS10); }

void gate_on_legato(uint8_t v) {
	if (v >= num_voices)
		return;

	pin_on(&voicetab[v].gate);
}

void gate_off(uint8_t v) {
	if (v >= num_voices)
		return;

	pin_off(&voicetab[v].gate);
}

void gate_on(uint8_t v) {
	gate_off(v);
	gate_on_legato(v);
}

void gate_init(void) {
	uint8_t v;

	for (v = 0; v < num_voices; v++) {
		pin *p = &voicetab[v].gate;
		pin_init(p);
		pin_off(p);
	}
}

struct env {
	/* phase is used to measure time since last update */
	uint16_t phase;
	/* period is the  "half life" of the envelope value */
	uint16_t period;
	/* slope sets exponentiation base to approx 1/2, 3/4, 7/8, etc. */
	uint8_t slope;
	int16_t value;
};

uint16_t env_decay(uint16_t value, uint8_t times) {
	uint16_t result = 0;

	for (; times; times--) {
		value >>= 1;
		result += value;
	}
	return result;
}

void env_advance(struct env *env, uint16_t now) {
	uint16_t value = env->value > 0 ? env->value : -env->value;

	if (env->period)
		for (; now - env->phase >= env->period;
		     env->phase += env->period)
			value = env_decay(value, env->slope + 1);
	else
		value = 0;

	env->value = env->value > 0 ? value : -value;
}

enum { MAX_VOICES = 8 }; /* Physical maximum number of voice cards */

struct {
	int16_t detune[MAX_VOICES];
	int16_t bend[MAX_VOICES];
	uint16_t base[MAX_VOICES];
	struct env env[MAX_VOICES];
	uint16_t *scale;
	uint8_t octave;
} pitch;

void pitch_init(void) {
	uint8_t v;

	for (v = 0; v < num_voices; v++) {
		pitch.bend[v] = 0;
		pitch.detune[v] = 0;
		pitch.env[v].period = 0;
	}
	pitch.scale = scale_12tet;
	pitch.octave = 0;
}

void clamp_add(uint16_t *x, int16_t y) {
	uint16_t z = *x + y;

	if (y > 0 && z < *x)
		z = UINT16_MAX;
	else if (y < 0 && z > *x)
		z = 0;
	*x = z;
}

void pitch_set_note(uint8_t voice, uint8_t note) {
	uint8_t octave = pitch.octave;

	if (voice >= num_voices || note >= NUM_NOTES)
		return;

	pitch.base[voice] = pitch.scale[note];
	while (octave--)
		clamp_add(&pitch.base[voice], dac_octave);
}

void pitch_set_bend(uint16_t bend) {
	enum { CENTER = 8192, SEMI = 2 };
	uint8_t v;
	uint8_t positive = bend > CENTER;
	uint16_t bend_amount = positive ? bend - CENTER : CENTER - bend;
	/* Exploit the fact that 8192 + 8192 / 64 + 8192 / 128 = 8384 is about 1
	 * octave on our DACs. */
	uint16_t bend_per_semi =
	    (bend_amount + bend_amount / 64 + bend_amount / 128) /
	    NUM_SEMITONES;
	uint16_t pitch_amount = SEMI * bend_per_semi;

	for (v = 0; v < num_voices; v++) {
		uint16_t max = SEMI * (dac_octave / (float)12);

		/* Both pitch_amount and max fit in int16_t */
		pitch.bend[v] = pitch_amount > max ? max : pitch_amount;
		if (!positive)
			pitch.bend[v] = -pitch.bend[v];
	}
}

void pitch_set_detune(uint8_t detune, uint8_t voices_per_key) {
	uint8_t v;

	if (!voices_per_key)
		voices_per_key = 1;
	for (v = 0; v < num_voices; v++) {
		pitch.detune[v] = detune >> ((v % voices_per_key) / 2);
		if (v % 2)
			pitch.detune[v] = -pitch.detune[v];
	}
}

void pitch_update(void) {
	uint8_t v;
	uint16_t now = TCNT1;

	for (v = 0; v < num_voices; v++) {
		uint16_t final_pitch = pitch.base[v];

		env_advance(&pitch.env[v], now);
		clamp_add(&final_pitch, pitch.env[v].value);
		clamp_add(&final_pitch, pitch.bend[v]);
		clamp_add(&final_pitch, pitch.detune[v]);
		dac_set(v, final_pitch);
	}
}

void chord_zero(list *chord, uint8_t len) {
	uint8_t i;

	l_set_len(chord, len);
	for (i = 0; i < chord->len; i++)
		chord->array[i] = 0;
}

void chord_normalize(list *chord) {
	uint8_t root, i;

	root = chord->array[0];
	for (i = 1; i < chord->len; i++)
		if (chord->array[i] < root)
			root = chord->array[i];

	for (i = 0; i < chord->len; i++)
		chord->array[i] -= root;
}

#define for_each_physical_voice(var, logical_voice, chord)                     \
	for (var = logical_voice * chord.len;                                  \
	     var < (logical_voice + 1) * chord.len; var++)

struct {
	uint8_t avail_array[MAX_VOICES];
	list avail;
	uint8_t in_use_array[MAX_VOICES];
	list in_use;
	uint8_t chord_array[MAX_VOICES];
	list chord;
	uint8_t next_chord_array[MAX_VOICES];
	list next_chord;
	uint8_t current_note[MAX_VOICES];
	uint8_t (*select_voice)(uint8_t note, uint8_t velocity);
	uint8_t detune;
	uint8_t damper_held;
	int16_t env_amount;
} poly = {{0}, INIT_LIST(poly.avail_array),
	  {0}, INIT_LIST(poly.in_use_array),
	  {0}, INIT_LIST(poly.chord_array),
	  {0}, INIT_LIST(poly.next_chord_array),
	  {0}, 0,
	  0,   0,
	  0};

uint8_t poly_logical_voices(void) { return num_voices / poly.chord.len; }

void poly_reinit(void) {
	uint8_t i;

	poly.in_use.len = 0;
	poly.avail.len = 0;
	for (i = 0; i < poly_logical_voices(); i++)
		l_push(&poly.avail, i);
	pitch_set_detune(poly.detune, poly.chord.len);
}

uint8_t poly_round_robin(uint8_t note, uint8_t velocity) {
	list *l = poly.avail.len ? &poly.avail : &poly.in_use;
	return l->array[--l->len];
}

void poly_init(void) {
	chord_zero(&poly.chord, 1);
	poly_reinit();
	poly.select_voice = poly_round_robin;
}

void poly_note_on(uint8_t key, uint8_t velocity) {
	uint8_t vl, vph, i;

	vl = poly.select_voice(key, velocity);
	l_push(&poly.in_use, vl);
	poly.current_note[vl] = key;
	i = 0;
	for_each_physical_voice (vph, vl, poly.chord) {
		pitch_set_note(vph, key + poly.chord.array[i++]);
		gate_on(vph);
		pitch.env[vph].value = poly.env_amount;
	}
}

void poly_apply_next_chord(void) {
	if (poly.in_use.len || poly.damper_held || !poly.next_chord.len)
		return;

	l_copy(&poly.chord, &poly.next_chord);
	poly.next_chord.len = 0;
	poly_reinit();
}

void poly_gate_off(uint8_t vl) {
	uint8_t vph;

	for_each_physical_voice (vph, vl, poly.chord)
		gate_off(vph);
}

void poly_note_off(uint8_t key) {
	uint8_t i, vl;

	for (i = 0; i < poly.in_use.len; i++) {
		vl = poly.in_use.array[i];
		if (poly.current_note[vl] != key)
			continue;

		l_delete_at(&poly.in_use, i);
		l_push(&poly.avail, vl);
		if (!poly.damper_held)
			poly_gate_off(vl);
	}

	poly_apply_next_chord();
}

enum {
	CC_DETUNE = 14,
	CC_PITCH_PERIOD = 15,
	CC_PITCH_AMOUNT = 16,
	CC_PITCH_BASE = 18,
	CC_STACK_VOICES = 17,
	CC_SUSTAIN = 64,
	CC_OCTAVE = 19,
	CC_SCALE = 70
};

void poly_control_change(uint8_t ctl, uint8_t val) {
	uint8_t i;

	switch (ctl) {
	case CC_STACK_VOICES:
		if (poly.in_use.len > 1) {
			l_set_len(&poly.next_chord, poly.in_use.len);
			for (i = 0; i < poly.next_chord.len; i++)
				poly.next_chord.array[i] =
				    poly.current_note[poly.in_use.array[i]];
			chord_normalize(&poly.next_chord);
		} else {
			chord_zero(&poly.next_chord, 1 << (val >> 5));
		}
		poly_apply_next_chord();
		break;
	case CC_DETUNE:
		poly.detune = val;
		pitch_set_detune(poly.detune, poly.chord.len);
		break;
	case CC_SUSTAIN:
		poly.damper_held = val & 0x40;
		if (!poly.damper_held)
			for (i = 0; i < poly.avail.len; i++)
				poly_gate_off(poly.avail.array[i]);
		poly_apply_next_chord();
		break;
	case CC_PITCH_PERIOD:
		for (i = 0; i < num_voices; i++)
			pitch.env[i].period = (uint16_t)val << 8;
		break;
	case CC_PITCH_AMOUNT:
		poly.env_amount = ((int16_t)val - 64) << 6;
		break;
	case CC_PITCH_BASE:
		for (i = 0; i < num_voices; i++)
			pitch.env[i].slope = (val >> 4);
		break;
	case CC_OCTAVE:
		pitch.octave = val;
		break;
	case CC_SCALE:
		pitch.scale = val >> 6 ? scale_31tet : scale_12tet;
		break;
	}
}

uint8_t poly_most_recently_used(uint8_t note, uint8_t velocity) {
	list *l = poly.avail.len ? &poly.avail : &poly.in_use;
	uint8_t v = l->array[0];

	l_delete_at(l, 0);
	return v;
}

void poly2_init(void) {
	poly_init();
	poly.select_voice = poly_most_recently_used;
}

uint8_t poly_prefer_same_voice(uint8_t note, uint8_t velocity) {
	uint8_t i, vl;

	i = 0;
	while (i < poly.avail.len &&
	       poly.current_note[poly.avail.array[i]] != note)
		i++;
	if (i == poly.avail.len)
		return poly_round_robin(note, velocity);

	vl = poly.avail.array[i];
	l_delete_at(&poly.avail, i);
	return vl;
}

void poly3_init(void) {
	poly_init();
	poly.select_voice = poly_prefer_same_voice;
}

uint8_t poly_voice_velocity(uint8_t note, uint8_t velocity) {
	uint8_t bands = poly.chord.len * (128 / num_voices);
	uint8_t vl = velocity / bands;
	l_delete(&poly.in_use, vl);
	l_delete(&poly.avail, vl);
	return vl;
}

void poly4_init(void) {
	poly_init();
	poly.select_voice = poly_voice_velocity;
}

uint8_t poly_voice_note(uint8_t note, uint8_t velocity) {
	uint8_t vl = note % poly_logical_voices();
	l_delete(&poly.in_use, vl);
	l_delete(&poly.avail, vl);
	return vl;
}

void poly5_init(void) {
	poly_init();
	poly.select_voice = poly_voice_note;
}

struct {
	uint8_t held_notes_array[NUM_NOTES];
	list held_notes;
	uint8_t chord_array[MAX_VOICES];
	list chord;
	void (*priority)(void);
} mono = {
    {0}, INIT_LIST(mono.held_notes_array), {0}, INIT_LIST(mono.chord_array), 0};

void mono_priority_last(void) {}

void swap(uint8_t *x, uint8_t *y) {
	uint8_t temp = *x;
	*x = *y;
	*y = temp;
}

void mono_priority_lowest(void) {
	uint8_t i;

	for (i = 0; i < mono.held_notes.len; i++)
		if (mono.held_notes.array[i] < mono.held_notes.array[0])
			swap(&mono.held_notes.array[i],
			     &mono.held_notes.array[0]);
}

void mono_priority_highest(void) {
	uint8_t i;

	for (i = 0; i < mono.held_notes.len; i++)
		if (mono.held_notes.array[i] > mono.held_notes.array[0])
			swap(&mono.held_notes.array[i],
			     &mono.held_notes.array[0]);
}

void mono_init(void) {
	mono.held_notes.len = 0;
	chord_zero(&mono.chord, mono.chord.cap);
	mono.priority = mono_priority_last;
}

void mono_set_pitch(void) {
	uint8_t n, i;

	mono.priority();
	n = mono.held_notes.array[0];
	for (i = 0; i < mono.chord.len; i++)
		pitch_set_note(i, n + mono.chord.array[i]);
}

void mono_note_on(uint8_t note, uint8_t velocity) {
	uint8_t i;

	l_delete(&mono.held_notes, note);
	l_push(&mono.held_notes, note);
	mono_set_pitch();
	for (i = 0; i < mono.chord.len; i++)
		gate_on_legato(i);
}

void mono_note_off(uint8_t n) {
	uint8_t v;

	l_delete(&mono.held_notes, n);
	if (mono.held_notes.len)
		mono_set_pitch();
	else
		for (v = 0; v < num_voices; v++)
			gate_off(v);
}

void mono_control_change(uint8_t ctl, uint8_t val) {
	switch (ctl) {
	case CC_STACK_VOICES:
		if (mono.held_notes.len > 1) {
			l_copy(&mono.chord, &mono.held_notes);
			chord_normalize(&mono.chord);
		} else {
			chord_zero(&mono.chord, 1 << ((127 - val) >> 5));
		}
		break;
	case CC_DETUNE:
		pitch_set_detune(val, num_voices);
		break;
	}
}

void mono2_init(void) {
	mono_init();
	mono.priority = mono_priority_lowest;
}

void mono3_init(void) {
	mono_init();
	mono.priority = mono_priority_highest;
}

void test_init(void) {
	uint8_t v;
	enum { test_note = 60 };

	pitch_init();

	for (v = 0; v < num_voices; v++) {
		gate_off(v);
		pitch_set_note(v, test_note);
	}
}

void test_note_on(uint8_t note, uint8_t velocity) {
	uint8_t v;

	for (v = 0; v < num_voices; v++)
		gate_off(v);
	gate_on(note);
}

void test_note_off(uint8_t n) { gate_off(n); }
void test_control_change(uint8_t cc, uint8_t val) {}

const struct {
	void (*init)(void);
	void (*note_on)(uint8_t n, uint8_t v);
	void (*note_off)(uint8_t n);
	void (*control_change)(uint8_t ctl, uint8_t val);
} programtab[] = {
    {poly_init, poly_note_on, poly_note_off, poly_control_change},
    {poly2_init, poly_note_on, poly_note_off, poly_control_change},
    {poly3_init, poly_note_on, poly_note_off, poly_control_change},
    {poly4_init, poly_note_on, poly_note_off, poly_control_change},
    {poly5_init, poly_note_on, poly_note_off, poly_control_change},
    {mono_init, mono_note_on, mono_note_off, mono_control_change},
    {mono2_init, mono_note_on, mono_note_off, mono_control_change},
    {mono3_init, mono_note_on, mono_note_off, mono_control_change},
    {test_init, test_note_on, test_note_off, test_control_change},
};

static uint8_t program = 0;

void program_change(uint8_t pgm) {
	enum { test_program = ARRAY_SIZE(programtab) - 1 };

	if (pgm < test_program) {
		program = pgm;
	} else if (pgm == 126) {
		/* For convenience, put a normal program right next to the test
		 * program, so that the user can quickly toggle back and forth
		 * between the two. */
		program = 0;
	} else if (pgm == 127) {
		/* Let the test program be selected via the last slot, to make
		 * it harder to select it by accident. */
		program = test_program;
	} else {
		/* Program index out of range */
		return;
	}

	gate_init();
	programtab[program].init();
}

struct {
	uint8_t array[16];
	uint8_t head;
	uint8_t tail;
} uart_buffer;

void uart_init(void) {
	enum { ubrr = (F_CPU / 16 / 31250) - 1 };
	UBRR0H = ubrr >> 8;
	UBRR0L = ubrr & 0xff;

	/* Enable UART0 RX and RX interrupt */
	UCSR0B = (1 << RXEN0) | (1 << RXCIE0);

	/* RX port pull-up */
	PORTD |= (1 << PORTD0);
}

ISR(USART_RX_vect) {
	uint8_t status = UCSR0A;
	uint8_t data = UDR0;

	if (!(status & (1 << RXC0)))
		return;

	if (status & ((1 << FE0) | (1 << DOR0) | (1 << UPE0)))
		return;

	uart_buffer.array[uart_buffer.head++] = data;
	uart_buffer.head %= ARRAY_SIZE(uart_buffer.array);
}

uint8_t uart_read(uint8_t *data) {
	if (uart_buffer.head == uart_buffer.tail)
		return 0;

	*data = uart_buffer.array[uart_buffer.tail++];
	uart_buffer.tail %= ARRAY_SIZE(uart_buffer.array);
	return 1;
}

void handle_channel_message(midi_message *msg) {
	if ((msg->status & 0xf) != config.midi_channel)
		return;

	switch (msg->status & 0xf0) {
	case MIDI_NOTE_ON:
		if (msg->data[1])
			programtab[program].note_on(msg->data[0], msg->data[1]);
		else
			programtab[program].note_off(msg->data[0]);
		break;
	case MIDI_NOTE_OFF:
		programtab[program].note_off(msg->data[0]);
		break;
	case MIDI_PROGRAM_CHANGE:
		program_change(msg->data[0]);
		break;
	case MIDI_CONTROL_CHANGE:
		programtab[program].control_change(msg->data[0], msg->data[1]);
		break;
	case MIDI_PITCH_BEND:
		pitch_set_bend((msg->data[1] << 7) | msg->data[0]);
		break;
	}
}

int main(void) {
	midi_parser parser = {0};

	uart_init();
	dac_init();
	config_init();
	timer_init();
	pitch_init();
	sei();

	program_change(0);

	for (;;) {
		uint8_t b;

		pitch_update();

		if (uart_read(&b)) {
			midi_message msg = midi_read(&parser, b);
			if (msg.status >= MIDI_SYSEX)
				config_handle_sysex(&msg);
			else if (msg.status)
				handle_channel_message(&msg);
		}
	}
}
