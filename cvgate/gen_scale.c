#include <stdint.h>
#include <stdio.h>

enum { NUM_NOTES = 128 }; /* Number of possible MIDI note values */

/* 8381.5 is the average octave scale on our hardware */
static const float dac_octave = 8381.5;

void print_scale(int notes, int skip) {
	int i, data;

	printf("uint16_t scale_%dtet[]={\n", notes);
	for (i = 0; i < skip; i++)
		printf("0, ");

	data = 0;
	for (; i < NUM_NOTES && data <= UINT16_MAX;
	     i++, data += dac_octave / (float)notes) {
		if (!((i - skip) % notes))
			data = ((i - skip) / notes) * dac_octave;
		printf("%d, ", data);
	}

	while (i++ < NUM_NOTES)
		printf("%d, ", UINT16_MAX);
	printf("};\n\n");
}

int main(void) {
	puts("#ifndef SCALE_H\n"
	     "#define SCALE_H\n"
	     "\n"
	     "#include <stdint.h>\n");
	printf("enum { NUM_NOTES = %d };\n\n", NUM_NOTES);
	printf("const float dac_octave = %g;\n\n", dac_octave);

	print_scale(12, 24);
	print_scale(31, 0);

	puts("#endif");
	return 0;
}
