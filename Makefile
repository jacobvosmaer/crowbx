CFLAGS = -Wall -Wextra -Werror -Wno-unused-parameter -std=gnu89 -pedantic

test: list-test config-test
	./list-test
	./config-test

list-test: list.c

config-test: CPPFLAGS += -DTEST=1 -Imidiparser/
config-test: CFLAGS += -g
config-test: config.c midi.o

midi.o: midiparser/midi.c
	$(CC) $(CFLAGS) -c -o $@ $^

clean:
	rm -f list-test config-test

format:
	clang-format -i -style=file *.c *.h
