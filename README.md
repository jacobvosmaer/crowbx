# CrowBX MIDI-CV converter firmware

This is firmware for a custom 8-voice polyphonic MIDI-CV converter
built into a CrowBX DIY synthesizer.

## Hardware

TODO: add schematic

The hardware consists of:

- an Arduino Pro Mini from Sparkfun
- an AD5676R 8-channel 16-bit DAC
- 2x LT1014 quad opamp to amplify the CV signals from the DAC
- a 6N137 optocoupler for the MIDI input
- regulators: 7812, 7912, 7805.

## Software

The MIDI channel can be set with
sysex messages of the following format, where `n` is the channel:

```
\xf0`CrowBX\x0n\xf7
```

Pitch bend has a range of +/- 2 semitones.

|MIDI program|Description|
|------------|-----------|
| 0 | poly mode (last note priority, round robin) |
| 1 | poly mode (first note priority)|
| 2 | poly mode (last note priority, sticky voice assignment)|
| 3 | mono unison (last note priority)|
| 4 | mono unison (lowest note priority)|
| 5 | mono unison (highest note priority)|
| 127 | autotune|

### Continuous Controllers

CC 14 adds detune in poly unison and mono unison modes.

In poly modes, CC 17 changes unison from 1 (full polyphony) to 2 (4
voice), 4 (2 voice) and 8 (1 voice).

#### Mono mode

In mono mode, CC 17 removes voices so you start with 8 voice unison,
then you get 7 voice, 6 voice, down to 1 voice mono mode. If you send
a CC 17 while holding down a chord, the currently held chord is
remembered so you can play that chord with one finger.

## License

MIT, see LICENSE.
