#include "array_size.h"
#include "list.h"
#include <assert.h>
#include <stdio.h>

static uint8_t test_list_array[10];
struct list test_list = {.array = test_list_array,
			 .cap = ARRAY_SIZE(test_list_array)};
struct list *l = &test_list;

int main(void) {
	l->len = 0;

	l_push(l, 2);
	l_push(l, 5);
	l_push(l, 3);
	assert(l->len == 3);
	assert(l->array[0] == 3);
	assert(l->array[1] == 5);
	assert(l->array[2] == 2);

	l_delete(l, 5);
	assert(l->len == 2);
	assert(l->array[0] == 3);
	assert(l->array[1] == 2);

	l_delete(l, 2);
	assert(l->len == 1);
	assert(l->array[0] == 3);

	l_push(l, 3);
	assert(l->len == 2);
	assert(l->array[0] == 3);
	assert(l->array[1] == 3);

	l_delete(l, 3);
	assert(l->len == 0);

	puts("ok");
}
