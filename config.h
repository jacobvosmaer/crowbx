#ifndef CONFIG_H
#define CONFIG_H

#include "midi.h"
#include <stdint.h>

struct config {
	uint8_t midi_channel;
};
extern struct config config;

void config_handle_sysex(midi_message *msg);
void config_init(void);

#endif
