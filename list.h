#ifndef LIST_H
#define LIST_H

#include "array_size.h"
#include <stdint.h>

typedef struct list {
	uint8_t len;
	uint8_t *const array;
	const uint8_t cap;
} list;

void l_push(list *l, uint8_t x);
void l_delete(list *l, uint8_t x);
void l_delete_at(list *l, uint8_t i);
void l_set_len(list *l, uint8_t len);
void l_copy(list *dst, list *src);

#define INIT_LIST(a)                                                           \
	{ 0, a, ARRAY_SIZE(a) }

#endif
